Short how to
===

* Get access to an account. If you want (need) one, send me an email at ```joachim<...something ...>giaever.org```.

*NOTE!* Regards to a new server and a new setup you will have to alter all of your existing ```./git/config``` to use the user ```git.giaever.org@git.giaever.org``` instead of ```giaever@git.giaever.org```.

# New things...
Regards to the change in software where we now support https and ssh with git, and every repository is managed on-site and not back-end through XML-RPC anymore. BY NOW: Use it as you would have used GitHub, and if you're stuck - search for it on Google. Happy go lucky!

## Where's my old repos?
- Dont worry! I will add them to the new site. Just be patient.


## Do I need keys now?
Yes you do, so please follow the steps:

* Generate a new key `$ ssh-keygen -t rsa -b 4096 -C "your@email.com`
* Complete the key generation and remember where your key is stored. Its normally stored in ~/.ssh/id_rsa, but for me who has keys on many different hosts normally gives each key a unique name, i.e `~/.ssh/id_git_giaever_org`.
* Create (or edit if exists) the config file for ssh: `$ vim ~/.ssh/config` and create the Host-entry in the file

```vim
Host git.giaever.org
    HostName git.giaever.org
    User git.giaever.org
    IdentityFile <my ssh key file>
```

* Ensure the permission to the config-file is correct: `chmod 0600 ~/.ssh/config`.
* Now print your public key: `$ cat <my ssh key file>.pub`
* Copy the printed content and paste that text where you [add your key](https://git.giaever.org/user/settings/ssh) (login required) by pressing CTRL+v. Please give the key a good description as you may have several keys on different computers.

Test your configuration, do:
```
$ ssh git.giaever.org@git.giaever.org
```
and ensure you get a response like:
```
PTY allocation request failed on channel 0
Hi there, You've successfully authenticated, but Gogs does not provide shell access.
If this is unexpected, please log in with password and setup Gogs under another user.
Connection to git.giaever.org closed.
```

# Outdated info:
* Clone your repo, to your local disk (```note``` the extra /git-directory):
```
$ git clone "repo url: either ssh or https>"
```
* Browse into the directory you just cloned, and update
```
$ cd ./<repo name>
$ git commit -m 'Initial Commit' --allow-empty
$ git push && git pull
```
* Set upstream to the branch your working on, which typically is ```master``` initially. (But whenevery you make a new branch, this should be executed as well - if you want to push it to remote.)
```
git push --set-upstream origin master
```
* If you havent set your username as global, you should set them for this repo. Use ```--global``` flag after config to set it globally, for every repo.
```
git config user.email "My@email"
git config user.name "My name"
```

* Then move your source files to the folder (or move the hidden .git folder from this folder, to where your source files is located), and do an initial commit from the directory containing the <.git folder>:
```
$ git add .
$ git commit -am "Added files to project"
$ git push
```
(Note the punctation after ```git add```, which adds all the files you have placed in this repo.)

## How to get an account
You need to be approved to get an GIT account here, and there is no such thing as a registration form, so dont bother look for it.

But if your looking for a place to [change your password](https://server.marthins1.no:20000/) your can do it [here](https://server.marthins1.no:20000/). But if you've forgotten it. Msg me ;)

## Why dont use Github?
Well. First of all it "cost money" (if you need several private repos), and if you have the knowlegde; why pay for it? :) Since I'm (and probably the users of this GIT) mostly can't share our code, its better to have our own solution which we can adapt to our needs.
