# Branching and merging

Its very convenient to create a branch, when you're gonna do changes to something that already "works" (often refered to as "the daily /driver/" or "prod" - the version in production). 

* [Read more about branching and merging](http://git-scm.com/book/en/v2/Git-Branching-Basic-Branching-and-Merging)

...or if you dont have time, and only need to recall the method - well here it is.

## Create a branch 
```
$ git branch <new branch name>
```
Ỳou have to be in the branch you want to make a copy of. You can see a list of all of your brancheds by issuing ```$ git branch``` and change to one of the listed ones with ```$ git checkout <existing branch name>```.

## Merge branches

Ok. Lets say you now for a while had a branch to fix up in something in a buggy code. Lets call it ```fixme``` which you branched from ```failure```. You dont need (or want) ```fixme``` within you branch tree anymore, and want to merge them together. 

1. Ensure your in the folder you want to merge within, which is failure; ```$ git checkout failure```, or you can issue ```$ git branch``` without any arguments - and it will highlight the branch you're on.
2. Than merge fixme with failure; ```$ git merge fixme```.

Hopefully everything works, and there arent any bugs in your code - lets rename it ```fixed```!
```
$ git branch -m fixed
```
## Delete a branch

Sometimes you want to delete a branch;
* Delete locally: ```$ git branch --delete <branch name>```
* Delete remote: ```$ git push origin --delete <branch name>```