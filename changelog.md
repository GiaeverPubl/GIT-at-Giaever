## Changelog

You can consider this as the news sector as well.

* 19. October 2015
	* Updated server and changed login type on web. Login as normal, but there may have been some troubles logging in lately.
	* Fixed bug about repo manager failing hence to a timeout.
	* Fixed the thumbnails in readme.
* 15. April 2015
	* Started using https instead of http. You can of course still use http in your repo, but it's not recommended. You should change from http to https in your config file, which is located in the .git-folder, of every repo.
	* Changed login mech.; no authentication with HTTP anymore.
* 14. April 2015
	* Finished the Repo manager.
